import {
  Component,
  OnInit
} from '@angular/core';

import { AppState } from '../app.service';
import { Title } from './title';
import { XLargeDirective } from './x-large';
import { Params } from '../state/params';
import { ErrorService } from '../state/state.error';
import { HttpService } from '../state/state.http';
import { LoginService } from '../state/state.login';
import {
  httpLoginComponent
} from './http/http.login.component';
import {
  Subscription
} from 'rxjs/Subscription';

@Component({
   selector: 'home',  // <home></home>
   providers: [
    Title
  ],
  styleUrls: [ './home.component.css' ],
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit
{
  private sUsername = 'patrick';
  private sPassword = 'password';

  private subErr: Subscription;
  private subHttp: Subscription;

  constructor(
    public servErr: ErrorService,
    public servHttp: HttpService,
    public service: LoginService
  )
  {
    console.log('HomeComponent::constructor() called.');
  }

  public ngOnInit()
  {
    console.log('HomeComponent::ngOnInit() called.');

    this.subErr = this.servErr.subscribe(message => {this.onError(message)});
    this.subHttp = this.servHttp.subscribe(message => {this.onHttp(message)});

  }

  private onError(message:any) : void
  {
    console.log('HomeComponent::OnError() called.');

    let error = this.servErr.get();

    alert(error.status + ' ' + error.statusText);

  }

  private onHttp(message: any) : void
  {
    console.log('HomeComponent::OnHttp() called.');

    let data = this.servHttp.get();

    alert(data.length);
  }

  private submitState(sUsername: string, sPassword: string)
  {
    console.log('HomeComponent::submitState() called');

    console.log('HomeComponent::submitState() username is', sUsername);
    console.log('HomeComponent::submitState() sPassword is', sPassword);

    var params = new Params(sUsername, sPassword);

    this.service.change(params);

  }

}
