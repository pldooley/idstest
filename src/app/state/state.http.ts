import {
  Injectable
} from '@angular/core';
import {
  Observable
} from 'rxjs';
import {
  Subject
} from 'rxjs/Subject';
import {
  EmitterService
} from './state.emitter';

@Injectable()
export class HttpService {

  static sTOPIC = 'httpService';

  private subject = new Subject<any>();
  private data: any;

  constructor()
  {
    console.log("HttpService::constructor() called");
  }

  public change(data: any)
  {
    console.log("HttpService::change() called");

    console.log("HttpService::change() data is ", data);

    this.data = data;

    this.sendMessage(HttpService.sTOPIC);
  }

  public get() : any
  {
    return this.data;
  }

  public subscribe (callback:any)
  {
    console.log('HttpService::constructor() called.');

    return this.getMessage().subscribe(callback);
  }

  private sendMessage(message: string)
  {
    console.log("HttpService::sendMessage() called");

    this.subject.next({ text: message });
  }

  private clearMessage()
  {
    console.log("HttpService::clearMessage() called");

    this.subject.next();
  }

  private getMessage(): Observable<any>
  {
    console.log("HttpService::getMessage() called");

    return this.subject.asObservable();
  }
}
