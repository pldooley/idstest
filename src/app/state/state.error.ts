import {
  Injectable
} from '@angular/core';
import {
  Observable
} from 'rxjs';
import {
  Subject
} from 'rxjs/Subject';
import {
  EmitterService
} from './state.emitter';

@Injectable()
export class ErrorService {

  static sTOPIC = 'errorService';

  private subject = new Subject<any>();
  private data: any;

  constructor()
  {
    console.log("ErrorService::constructor() called");
  }

  public change(data: any)
  {
    console.log("ErrorService::change() called");

    console.log("ErrorService::change() data is ", data);

    this.data = data;

    this.sendMessage(ErrorService.sTOPIC);
  }

  public get() : any
  {
    return this.data;
  }

  public subscribe (callback:any)
  {
    console.log('ErrorService::constructor() called.');

    return this.getMessage().subscribe(callback);
  }

  private sendMessage(message: string)
  {
    console.log("ErrorService::sendMessage() called");

    this.subject.next({ text: message });
  }

  private clearMessage()
  {
    console.log("ErrorService::clearMessage() called");

    this.subject.next();
  }

  private getMessage(): Observable<any>
  {
    console.log("ErrorService::getMessage() called");

    return this.subject.asObservable();
  }
}
