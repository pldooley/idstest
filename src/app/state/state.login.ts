import {
  Injectable
} from '@angular/core';
import {
  Observable
} from 'rxjs';
import {
  Subject
} from 'rxjs/Subject';
import {
  EmitterService
} from './state.emitter';
import {
  Params
} from './params';

@Injectable()
export class LoginService {

  static sTOPIC = 'loginService';

  private subject = new Subject<any>();
  private params: Params;

  constructor()
  {
    console.log("LoginService::constructor() called");
  }

  public change(params: Params)
  {
    console.log("LoginService::change() called");

    console.log("LoginService::change() params is ", params);

    this.params = params;

    this.sendMessage(LoginService.sTOPIC);
  }

  public get() : Params
  {
    return this.params;
  }

  public subscribe (callback:any)
  {
    console.log('LoginService::constructor() called.');

    return this.getMessage().subscribe(callback);
  }

  private sendMessage(message: string)
  {
    console.log("LoginService::sendMessage() called");

    this.subject.next({ text: message });
  }

  private clearMessage()
  {
    console.log("LoginService::clearMessage() called");

    this.subject.next();
  }

  private getMessage(): Observable<any>
  {
    console.log("LoginService::getMessage() called");

    return this.subject.asObservable();
  }


}
