import {
  Injectable
} from '@angular/core';
import {
  Observable
} from 'rxjs';
import {
  Subject
} from 'rxjs/Subject';
import {
  AppStatus
} from './enumApp';
import {
  EmitterService
} from './state.emitter';

@Injectable()
export class AppService {

  static sTOPIC = 'AppService';

  private subject = new Subject<any>();
  private eStatus: number = AppStatus.loggedOut;

  constructor()
  {
    console.log("AppService::constructor() called");
  }

  public change(eStatus: number)
  {
    console.log("AppService::change() called");

    console.log("AppService::change() eStatus is ", eStatus);

    this.eStatus = eStatus;

    this.sendMessage(AppService.sTOPIC);
  }

  public get() : number
  {
    return this.eStatus;
  }

  public subscribe (callback:any)
  {
    console.log('LoginService::constructor() called.');

    return this.getMessage().subscribe(callback);
  }

  private sendMessage(message: string)
  {
    console.log("LoginService::sendMessage() called");

    this.subject.next({ text: message });
  }

  private clearMessage()
  {
    console.log("LoginService::clearMessage() called");

    this.subject.next();
  }

  private getMessage(): Observable<any>
  {
    console.log("LoginService::getMessage() called");

    return this.subject.asObservable();
  }


}
