import {
  Component,
  OnDestroy
} from '@angular/core';
import {
  Http,
  Response,
  Headers,
  RequestOptions
} from '@angular/http';
import {
  Param
} from '../state/param';
import {
  Observable
} from 'rxjs/Rx';
import {
  Subscription
} from 'rxjs/Subscription';
import {
  LoginService
} from '../state/state.login';
import {
  ErrorService
} from '../state/state.error';
import {
  HttpService
} from '../state/state.http';
import {
  Params
} from '../state/params';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Component({
  selector: 'http-login',
  template: ``
})

export class httpLoginComponent implements OnDestroy
{
  //static sLOGIN_URL = 'http://localhost:3000/api/comments';
  static sLOGIN_URL = 'http://localhost:3003/api/accounts';

  private message: any;
  private subscription: Subscription;

  constructor (
    private http: Http,
    private loginService: LoginService,
    private errorService: ErrorService,
    private httpService: HttpService )
  {
    console.log('httpLoginComponent::constructor() called.');

    this.subscription = this.loginService.subscribe(message => {this.onLogin(message)});
  }

  //private onLogin: (message:any) => void = (message:any) =>
  private onLogin(message:any): void
  {
    console.log('httpLoginComponent::onLogin() called.');
    console.log('httpLoginComponent::onLogin() message is ', message);

    let params: Params = this.loginService.get();
    console.log('httpLoginComponent::onLogin() params is ', params);

    this.get();
    //this.post(params);

  }

  private get()
  {
    console.log('httpLoginComponent::get() called.');

    return this.http.get(httpLoginComponent.sLOGIN_URL)
      .map((res:Response) => res.json())
      .catch(err => this.handleError(err, this.errorService))
      .subscribe(result => this.httpService.change(result));
  }

  private handleError(err: any, srvc: any)
  {
    console.log('httpLoginComponent::handleError() called.');

    console.debug("httpLoginComponent::handleError() err is ", err);

    srvc.change(err);
    return Observable.throw(err);
  }

  private post(params: Params) : Observable<Comment[]>
  {
    console.log('httpLoginComponent::post() called.');

    let sBody       = JSON.stringify(params);
    let headers     = new Headers({ 'Content-Type': 'application/json' });
    let options     = new RequestOptions({ headers: headers });

    return this.http.post(httpLoginComponent.sLOGIN_URL, sBody, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  public ngOnDestroy()
  {
    console.log('httpLoginComponent::ngOnDestroy() called.');

    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }

}
